package com.example.jobinterviewproject24i

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.core.FetchMediaUseCase
import com.example.core.model.Media
import com.example.jobinterviewproject24i.base.EmptyState
import com.example.jobinterviewproject24i.base.LoadingState
import com.example.jobinterviewproject24i.base.ReadyState
import com.example.jobinterviewproject24i.base.ViewModelState
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.BackpressureStrategy
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers.trampoline
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class MediaListViewModelUnitTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val fetchMediaUseCase: FetchMediaUseCase = mock()
    private val fetchMediaExceptionFactory: FetchMediaException.Factory = mock()
    private val stateObserver: Observer<ViewModelState> = mock()
    private val dataObserver: Observer<List<Media>> = mock()

    private val mockMedia = listOf(Media("MockName", "MockUrl"))

    private lateinit var viewModel: MediaListViewModel

    private val mediaDetailFetchedPublishSubject = PublishSubject.create<List<Media>>()
    private val mediaUpdatesFetchedPublishSubject = PublishSubject.create<Int>()

    @Before
    fun setUpViewModel() {

        RxJavaPlugins.setIoSchedulerHandler { trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { trampoline() }

        Mockito
            .`when`(fetchMediaUseCase.getFetchingMediaObservable())
            .thenReturn(mediaDetailFetchedPublishSubject.toFlowable(BackpressureStrategy.LATEST))

        Mockito
            .`when`(fetchMediaUseCase.startFetchingMedia())
            .thenReturn(mediaUpdatesFetchedPublishSubject.singleOrError())

        viewModel = MediaListViewModel(fetchMediaUseCase, fetchMediaExceptionFactory)
        viewModel.state.observeForever(stateObserver)
        viewModel.data.observeForever(dataObserver)
    }

    @Test
    fun initializeSetsLoadingState() {

        verify(stateObserver).onChanged(LoadingState())
    }

    @Test
    fun retrySetsLoadingState() {

        viewModel.retryMediaFetching()

        verify(stateObserver, times(2)).onChanged(LoadingState())
    }

    @Test
    fun onUpdatesFetchedSetsReadyState() {

        mediaUpdatesFetchedPublishSubject.onNext(1)
        mediaUpdatesFetchedPublishSubject.onComplete()

        verify(stateObserver).onChanged(ReadyState())
    }

    @Test
    fun onEmptyUpdatesFetchedSetsEmptyState() {

        mediaUpdatesFetchedPublishSubject.onNext(0)
        mediaUpdatesFetchedPublishSubject.onComplete()

        verify(stateObserver).onChanged(EmptyState())
    }

    @Test
    fun onUpdatesFetchingErrorSetsEmptyState() {

        mediaUpdatesFetchedPublishSubject.onError(Exception())

        verify(stateObserver).onChanged(EmptyState())
    }

    @Test
    fun onMediaFetchedUpdatesData() {

        mediaDetailFetchedPublishSubject.onNext(mockMedia)

        verify(dataObserver).onChanged(mockMedia)
    }

}