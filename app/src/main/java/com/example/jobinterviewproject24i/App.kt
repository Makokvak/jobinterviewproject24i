package com.example.jobinterviewproject24i

import android.app.Application

@Suppress("unused")
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        AppKoinModule.startKoin(this)
    }
}