package com.example.jobinterviewproject24i

import android.app.Application
import com.example.core.CoreKoinModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

object AppKoinModule {

    private val module = module {

        single { FetchMediaException.Factory(get()) }
        viewModel { MediaListViewModel(get(), get()) }
    }

    fun startKoin(application: Application) {

        startKoin {
            androidContext(application)
            androidLogger(Level.DEBUG)
            module { }
        }

        CoreKoinModule.start()
        loadKoinModules(module)
    }

}