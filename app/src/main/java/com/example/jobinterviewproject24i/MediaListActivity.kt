package com.example.jobinterviewproject24i

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.example.core.model.Media
import com.example.jobinterviewproject24i.base.ActivityBase
import com.example.jobinterviewproject24i.base.GenericRecyclerViewAdapter
import com.example.jobinterviewproject24i.databinding.ActivityMediaListBinding
import com.example.jobinterviewproject24i.databinding.ItemMediaBinding

typealias MediaAdapter = GenericRecyclerViewAdapter<Media, ItemMediaBinding>

class MediaListActivity : ActivityBase<MediaListViewModel, ActivityMediaListBinding>(
    R.layout.activity_media_list,
    MediaListViewModel::class
) {

    private var lastAnimatedViewPosition = Int.MIN_VALUE

    private val adapter: MediaAdapter by lazy {
        object : MediaAdapter(this, viewModel.data, R.layout.item_media) {
            override fun populateViews(binding: ItemMediaBinding, data: Media, position: Int) =
                this@MediaListActivity.populateViews(binding, data, position)
        }
    }

    private val layoutManager = GridLayoutManager(this, 2)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bindErrorMessages()
        initializeRecyclerView()
        bindClickListeners()
    }

    private fun populateViews(binding: ItemMediaBinding, data: Media, position: Int) {

        binding.textViewTitle.text = data.title

        val coverUrl = data.coverImageUrl
        if (coverUrl != null)
            Glide.with(this)
                .load(coverUrl)
                .placeholder(R.mipmap.placeholder)
                .into(binding.imageViewCover)

        animateListItem(binding.container, position)
    }

    private fun initializeRecyclerView() {
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    private fun bindErrorMessages() =
        viewModel.errorOccurredSignal.observeOnce(this, Observer { error ->
            showErrorMessageFor(error)
        })

    private fun bindClickListeners() =
        binding.buttonRetry.setOnClickListener {
            viewModel.retryMediaFetching()
        }

    private fun animateListItem(viewToAnimate: View, position: Int) {

        val shouldAnimateView = position > lastAnimatedViewPosition
        if (shouldAnimateView) {
            val animation = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastAnimatedViewPosition = position
        }
    }

}
