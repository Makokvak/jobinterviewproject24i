package com.example.jobinterviewproject24i.base

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class ViewModelState

class InitialState : ViewModelState() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}

class LoadingState : ViewModelState() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}

class ReadyState : ViewModelState() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}

class EmptyState : ViewModelState() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}

data class ErrorState(val error: Throwable) : ViewModelState() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}

@Suppress("unused")
open class ViewModelBase : ViewModel() {

    protected val compositeDisposable = CompositeDisposable()
    private val stateBackStack: MutableList<ViewModelState> = mutableListOf()
    val previousState: MutableLiveData<ViewModelState> = MutableLiveData()
    val state: SafeMutableLiveData<ViewModelState> =
        SafeMutableLiveData(InitialState())

    private val inLoadingState = SafeMutableLiveData(false)
    private val inReadyState = SafeMutableLiveData(false)
    private val inErrorState = SafeMutableLiveData(false)
    private val inEmptyState = SafeMutableLiveData(false)
    private val inInitialState = SafeMutableLiveData(true)

    val isInInitialState: SafeLiveData<Boolean> = inInitialState
    val isInLoadingState: SafeLiveData<Boolean> = inLoadingState
    val isInEmptyState: SafeLiveData<Boolean> = inEmptyState
    val isInReadyState: SafeLiveData<Boolean> = inReadyState
    val isInErrorState: SafeLiveData<Boolean> = inErrorState

    val isNotInInitialState: SafeLiveData<Boolean> = inInitialState.map { value: Boolean -> !value }
    val isNotInLoadingState: SafeLiveData<Boolean> = inLoadingState.map { value: Boolean -> !value }
    val isNotInEmptyState: SafeLiveData<Boolean> = inEmptyState.map { value: Boolean -> !value }
    val isNotInReadyState: SafeLiveData<Boolean> = inReadyState.map { value: Boolean -> !value }
    val isNotInErrorState: SafeLiveData<Boolean> = inErrorState.map { value: Boolean -> !value }

    init {

        state.observeForever {
            stateBackStack.add(it)

            val localPreviousState = fetchPreviousState()

            if (localPreviousState != previousState.value)
                previousState.value = localPreviousState

            when (it) {
                is InitialState -> {
                    inInitialState.value = true
                }
                is LoadingState -> {
                    inLoadingState.value = true
                }
                is EmptyState -> {
                    inEmptyState.value = true
                }
                is ReadyState -> {
                    inReadyState.value = true
                }
                is ErrorState -> {
                    inErrorState.value = true
                }
            }
        }

        previousState.observeForever {
            when (it) {
                is InitialState -> {
                    inInitialState.value = false
                }
                is LoadingState -> {
                    inLoadingState.value = false
                }
                is EmptyState -> {
                    inEmptyState.value = false
                }
                is ReadyState -> {
                    inReadyState.value = false
                }
                is ErrorState -> {
                    inErrorState.value = false
                }
            }
        }
    }

    private fun fetchPreviousState(): ViewModelState? {
        return stateBackStack.getOrNull(stateBackStack.size - 2)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    companion object {

        @BindingAdapter("visible")
        @JvmStatic
        fun setVisible(view: View, safeLiveData: SafeLiveData<Boolean>) {

            val activity = view.context as? AppCompatActivity

            if (activity != null) {
                safeLiveData.observe(activity, Observer { visible ->
                    view.visibility = if (visible) View.VISIBLE else View.GONE
                })
            }

            view.visibility = if (safeLiveData.getValue()) View.VISIBLE else View.GONE
        }

    }

}