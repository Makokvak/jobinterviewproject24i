package com.example.jobinterviewproject24i.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class GenericRecyclerViewAdapter<TItem, ItemViewDataBinding : ViewDataBinding>(
    lifecycleOwner: LifecycleOwner,
    @Suppress("MemberVisibilityCanBePrivate") val data: SafeMutableLiveData<out List<TItem>>,
    @LayoutRes private val itemLayoutResourceId: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class GenericRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private var previousList = data.value.toList()

    private val listChangeObserver = Observer<List<TItem>> {
        val diffResult = DiffUtil.calculateDiff(GenericDiffCallback(previousList, it))
        diffResult.dispatchUpdatesTo(this)
        previousList = it.toList()
    }

    init {
        data.observe(lifecycleOwner, listChangeObserver)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        GenericRecyclerViewHolder(LayoutInflater.from(parent.context).inflate(itemLayoutResourceId, parent, false))

    override fun getItemCount(): Int = data.value.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val binding = DataBindingUtil.bind<ItemViewDataBinding>(holder.itemView) ?: return

        populateViews(binding, data.value[position], position)
    }

    protected open fun populateViews(binding: ItemViewDataBinding, data: TItem, position: Int) {}

    @Suppress("unused")
    open fun add(item: TItem, position: Int = itemCount) {

        val listCopy = data.value.toMutableList()
        listCopy.add(position, item)
        data.value = listCopy
    }

    @Suppress("unused")
    open fun addRange(itemsToAdd: Collection<TItem>, position: Int = itemCount) {

        val listCopy = data.value.toMutableList()
        listCopy.addAll(position, itemsToAdd)
        data.value = listCopy
    }

    @Suppress("unused")
    open fun remove(item: TItem) {

        val position = data.value.indexOf(item)
        if (position < 0) return

        removeAt(position)
    }

    open fun removeAt(index: Int) {

        if (index >= data.value.size || index < 0) return

        val listCopy = data.value.toMutableList()
        listCopy.removeAt(index)
        data.value = listCopy
    }

    open fun clear() {
        data.value = listOf()
    }
}
