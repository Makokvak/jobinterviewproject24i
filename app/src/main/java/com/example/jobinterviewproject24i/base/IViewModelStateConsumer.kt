package com.example.jobinterviewproject24i.base

interface IViewModelStateConsumer {
    fun onLoadingStateEnter()
    fun onLoadingStateExit()
    fun onInitialStateEnter()
    fun onInitialStateExit()
    fun onEmptyStateEnter()
    fun onEmptyStateExit()
    fun onReadyStateExit()
    fun onReadyStateEnter()
    fun onErrorStateEnter(error: Throwable)
    fun onErrorStateExit()
}
