package com.example.jobinterviewproject24i.base

import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.jobinterviewproject24i.BR
import com.example.jobinterviewproject24i.R
import org.koin.android.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class ActivityBase<
        TViewModel : ViewModelBase,
        TDataBinding : ViewDataBinding>(
    @LayoutRes private val layoutResourceId: Int,
    viewModelClass: KClass<TViewModel>
) : AppCompatActivity(), IViewModelStateObserver by ViewModelStateObserver(), IViewModelStateConsumer {

    protected val viewModel: TViewModel by lazy {
        getViewModel(viewModelClass)
    }

    lateinit var binding: TDataBinding; private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, layoutResourceId)
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = this

        initializeStateObserver(this, viewModel, this)
    }

    override fun onLoadingStateEnter() {
    }

    override fun onLoadingStateExit() {
    }

    override fun onInitialStateEnter() {
    }

    override fun onInitialStateExit() {
    }

    override fun onEmptyStateEnter() {
    }

    override fun onEmptyStateExit() {
    }

    override fun onReadyStateExit() {
    }

    override fun onReadyStateEnter() {
    }

    override fun onErrorStateEnter(error: Throwable) {
    }

    override fun onErrorStateExit() {
    }

    protected fun showErrorMessageFor(error: Throwable) =
        makeToast(error.localizedMessage).show()

    protected open fun makeToast(message: String): Toast =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).apply {
            view.setBackgroundResource(R.drawable.toast_background)
            view.findViewById<TextView>(android.R.id.message).setTextColor(ContextCompat.getColor(this@ActivityBase, R.color.primaryTextLight))
        }

}