package com.example.jobinterviewproject24i.base

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

interface SafeLiveData<T> {

    fun getValue(): T
    fun observe(owner: LifecycleOwner, observer: Observer<in T>)
    fun observeForever(observer: Observer<in T>)

}

class SafeMutableLiveData<T : Any>(initialValue: T) : MutableLiveData<T>(),
    SafeLiveData<T> {

    init {
        value = initialValue
    }

    override fun getValue(): T {
        return super.getValue()!!
    }

}

class SafeMediatorLiveData<T : Any>(initialValue: T) : MediatorLiveData<T>(),
    SafeLiveData<T> {

    init {
        value = initialValue
    }

    override fun getValue(): T {
        return super.getValue()!!
    }

}

@Suppress("unused")
@MainThread
fun <T : Any> SafeMutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

@MainThread
fun <TValue : Any> SafeMutableLiveData<TValue>.map(
    mapFunction: (TValue) -> TValue
): SafeLiveData<TValue> {

    val result = SafeMediatorLiveData(this.value)
    result.addSource(this) { x -> result.value = mapFunction(x) }
    return result
}