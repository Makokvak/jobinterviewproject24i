package com.example.jobinterviewproject24i.base

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer


class ViewModelStateObserver : IViewModelStateObserver {

    override fun initializeStateObserver(lifecycleOwner: LifecycleOwner, viewModel: ViewModelBase, stateConsumer: IViewModelStateConsumer) {

        viewModel.state.observe(lifecycleOwner, Observer {
            when (it) {
                is InitialState -> {
                    stateConsumer.onInitialStateEnter()
                }
                is LoadingState -> {
                    stateConsumer.onLoadingStateEnter()
                }
                is EmptyState -> {
                    stateConsumer.onEmptyStateEnter()
                }
                is ReadyState -> {
                    stateConsumer.onReadyStateEnter()
                }
                is ErrorState -> {
                    stateConsumer.onErrorStateEnter(it.error)
                }
            }
        })

        viewModel.previousState.observe(lifecycleOwner, Observer {
            when (it) {
                is InitialState -> {
                    stateConsumer.onInitialStateExit()
                }
                is LoadingState -> {
                    stateConsumer.onLoadingStateExit()
                }
                is EmptyState -> {
                    stateConsumer.onEmptyStateExit()
                }
                is ReadyState -> {
                    stateConsumer.onReadyStateExit()
                }
                is ErrorState -> {
                    stateConsumer.onErrorStateExit()
                }
            }
        })
    }

}