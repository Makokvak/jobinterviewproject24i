package com.example.jobinterviewproject24i.base

import androidx.lifecycle.LifecycleOwner

interface IViewModelStateObserver {
    fun initializeStateObserver(lifecycleOwner: LifecycleOwner, viewModel: ViewModelBase, stateConsumer: IViewModelStateConsumer)
}