package com.example.jobinterviewproject24i

import android.content.Context

// can be easily refactored to differentiate among multiple error states - currently this one error represents any `media data fetch` error, which is not perfect
class FetchMediaException(private val context: Context) : Exception() {

    override fun getLocalizedMessage(): String {
        return context.getString(R.string.failed_to_fetch_media)
    }

    class Factory(private val context: Context) {
        fun create() = FetchMediaException(context)
    }

}