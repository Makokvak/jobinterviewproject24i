package com.example.jobinterviewproject24i

import com.example.core.FetchMediaUseCase
import com.example.core.model.Media
import com.example.jobinterviewproject24i.base.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class MediaListViewModel(
    private val fetchMediaUseCase: FetchMediaUseCase,
    private val fetchMediaExceptionFactory: FetchMediaException.Factory
) : ViewModelBase() {

    val data = SafeMutableLiveData(listOf<Media>())
    val errorOccurredSignal = SingleEventLiveData<Throwable>()

    init {
        observeMediaUpdates()
        fetchMedia()
    }

    fun retryMediaFetching() {
        fetchMedia()
    }

    private fun fetchMedia() {

        fetchMediaUseCase.cancelAllMediaFetching()

        state.value = LoadingState()

        fetchMediaUseCase
            .startFetchingMedia()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                it.printStackTrace()
                errorOccurredSignal.value = fetchMediaExceptionFactory.create()
                state.value = EmptyState()
            }
            .subscribe({ countOfMediaThatStartedFetching ->
                state.value = if (countOfMediaThatStartedFetching > 0) ReadyState() else EmptyState()
            }, {})
            .addTo(compositeDisposable)
    }

    private fun observeMediaUpdates() {
        fetchMediaUseCase.getFetchingMediaObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::updateMediaData) {}
            .addTo(compositeDisposable)
    }

    private fun updateMediaData(dataUpdates: List<Media>) {
        val currentData = data.value
        val newData = dataUpdates.toMutableList().subtract(currentData)

        val updatedData = currentData.toMutableList().apply {
            addAll(newData)
        }
        data.value = updatedData
    }

}
