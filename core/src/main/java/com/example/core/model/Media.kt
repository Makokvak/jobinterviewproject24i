package com.example.core.model

import com.example.core.BuildConfig
import com.example.webservice.response.FetchMediaDetailResponse

data class Media(val title: String, val coverImageUrl: String?) {

    companion object {

        /**
         * Converts webservice layer model to app layer model
         *
         * @param response webservice layer media model
         * @return app layer media model
         */
        fun fromMediaDetailResponse(response: FetchMediaDetailResponse): Media =
            Media(
                response.title,
                if (response.coverImageUrl != null) BuildConfig.IMAGES_URL + response.coverImageUrl else null
            )
    }

}
