package com.example.core.worker

import android.content.Context
import androidx.work.Data
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.core.model.Media
import com.example.webservice.MediaWebservice
import com.example.webservice.response.FetchMediaDetailResponse
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.inject

internal class MediaDetailRequestWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : RxWorker(appContext, workerParams), KoinComponent {

    companion object {
        const val WORK_TAG = "media_detail_request_work_tag"
        private const val MOVIE_DETAIL_RESPONSE_KEY = "movie_detail_response"
        private const val MOVIE_ID_PARAMETER_KEY = "movie_id_parameter_key"

        fun makeInputData(movieId: Long): Data {
            val builder = Data.Builder()
            builder.putLong(MOVIE_ID_PARAMETER_KEY, movieId)
            return builder.build()
        }

        /**
         * Retrieves Media app layer model from Data object
         *
         * @throws IllegalArgumentException if [MOVIE_DETAIL_RESPONSE_KEY] key was not found in provided data
         * @throws JsonSyntaxException if [MOVIE_DETAIL_RESPONSE_KEY] parameter in data provided failed to deserialize into JSON
         *
         * @return Media model that is a result of this worker or null if the data is marked as adult.
         */
        fun getOutputNonAdultData(data: Data): Media? {

            val responseJson = data.getString(MOVIE_DETAIL_RESPONSE_KEY)
                ?: throw IllegalArgumentException("$MOVIE_DETAIL_RESPONSE_KEY was not found in provided data")

            val response = deserializeFromJson(responseJson)

            if (response.isAdult)
                return null

            return Media.fromMediaDetailResponse(response)
        }

        private fun serializeToJson(response: FetchMediaDetailResponse) = Gson().toJson(response)

        private fun deserializeFromJson(json: String): FetchMediaDetailResponse =
            Gson().fromJson(json, FetchMediaDetailResponse::class.java)

    }

    override fun createWork(): Single<Result> {

        val mediaWebservice: MediaWebservice by inject()
        val idOfMediaToFetch = inputData.getLong(MOVIE_ID_PARAMETER_KEY, 0)

        return mediaWebservice
            .fetchMediaDetail(idOfMediaToFetch)
            .map { response ->
                val responseSerializedToJson = serializeToJson(response)
                val workerOutputData = workDataOf(MOVIE_DETAIL_RESPONSE_KEY to responseSerializedToJson)
                Result.success(workerOutputData)
            }
            .onErrorReturn {
                Result.retry()
            }
    }

}
