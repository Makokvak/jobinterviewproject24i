package com.example.core

import androidx.work.Data
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.example.core.model.Media
import com.example.core.worker.MediaDetailRequestWorker
import com.example.webservice.MediaWebservice
import com.example.webservice.response.FetchMediaUpdatesResponse
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

class FetchMediaUseCaseImpl(
    private val mediaWebservice: MediaWebservice,
    private val webserviceWorkScheduler: WebserviceWorkScheduler
) : FetchMediaUseCase {

    companion object {
        private const val NUMBER_OF_UPDATES_FETCH_LIMIT = 30
    }

    private val fetchingMediaPublishSubject = PublishSubject.create<List<Media>>()

    init {
        observeMediaDetailWorkStates()
    }

    override fun startFetchingMedia(forLastDays: Int): Single<Int> =
        validateLastDaysWithinRange(forLastDays)
            .andThen(mediaWebservice.fetchMediaUpdates())
            .flatMap { scheduleMediaDetailRequests(it) }

    override fun getFetchingMediaObservable(): Flowable<List<Media>> =
        fetchingMediaPublishSubject.toFlowable(BackpressureStrategy.LATEST)

    override fun cancelAllMediaFetching() {
        WorkManager.getInstance().apply {
            cancelAllWorkByTag(MediaDetailRequestWorker.WORK_TAG)
            pruneWork()
        }
    }

    private fun validateLastDaysWithinRange(forLastDays: Int): Completable =
        Completable.fromCallable {
            if (forLastDays < 1)
                throw IllegalArgumentException("forLastDays must be greater than zero, received: $forLastDays")

            if (forLastDays > FetchMediaUseCase.MAX_DAYS_TO_FETCH_MEDIA_UPDATES_FOR)
                throw IllegalArgumentException("forLastDays must be lower than ${FetchMediaUseCase.MAX_DAYS_TO_FETCH_MEDIA_UPDATES_FOR}, received: $forLastDays")
        }

    /**
     * @return count of scheduled requests
     */
    private fun scheduleMediaDetailRequests(fetchMediaUpdatesResponse: FetchMediaUpdatesResponse): Single<Int> =
        Single.fromCallable {
            val mediaToFetchDetailsFor = fetchMediaUpdatesResponse.results.subList(0, NUMBER_OF_UPDATES_FETCH_LIMIT)
            mediaToFetchDetailsFor.forEach {
                webserviceWorkScheduler.addMovieDetailRequest(it.id)
            }
            return@fromCallable mediaToFetchDetailsFor.size
        }

    private fun observeMediaDetailWorkStates() =
        WorkManager.getInstance().getWorkInfosByTagLiveData(MediaDetailRequestWorker.WORK_TAG)
            .observeForever { workInfos ->
                WorkManager.getInstance().pruneWork()
                workInfos
                    .filter { it.state == WorkInfo.State.SUCCEEDED }
                    .map { it.outputData }
                    .apply { processSucceededMediaDetailData(this) }
            }

    private fun processSucceededMediaDetailData(workData: List<Data>) =
        workData
            .mapNotNull { MediaDetailRequestWorker.getOutputNonAdultData(it) }
            .apply { fetchingMediaPublishSubject.onNext(this) }
    
}