package com.example.core

import androidx.work.BackoffPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.core.worker.MediaDetailRequestWorker
import java.util.concurrent.TimeUnit

class WebserviceWorkScheduler {

    fun addMovieDetailRequest(movieId: Long) =
        WorkManager.getInstance().enqueue(makeMovieDetailRequestWorker(movieId))

    private fun makeMovieDetailRequestWorker(movieId: Long): OneTimeWorkRequest =
        OneTimeWorkRequestBuilder<MediaDetailRequestWorker>()
            .addTag(MediaDetailRequestWorker.WORK_TAG)
            .setInputData(MediaDetailRequestWorker.makeInputData(movieId))
            .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 10, TimeUnit.SECONDS)
            .build()

}