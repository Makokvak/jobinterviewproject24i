package com.example.core

import com.example.webservice.WebserviceKoinModule
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object CoreKoinModule {

    private val module = module {
        single { WebserviceWorkScheduler() }
        single<FetchMediaUseCase> { FetchMediaUseCaseImpl(get(), get()) }
    }

    fun start() {
        WebserviceKoinModule.start()
        loadKoinModules(module)
    }

}