package com.example.core

import com.example.core.model.Media
import io.reactivex.Flowable
import io.reactivex.Single

interface FetchMediaUseCase {

    companion object {
        internal const val MAX_DAYS_TO_FETCH_MEDIA_UPDATES_FOR = 14
    }

    /**
     * @return count of updates matching the parameter
     */
    fun startFetchingMedia(forLastDays: Int = MAX_DAYS_TO_FETCH_MEDIA_UPDATES_FOR): Single<Int>

    /**
     * @return Observable with all media that are successfully fetched. Each update will contain ALL successfully fetched media.
     */
    fun getFetchingMediaObservable(): Flowable<List<Media>>

    fun cancelAllMediaFetching()

}