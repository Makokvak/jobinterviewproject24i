package com.example.webservice

import com.example.webservice.retrofit.RetrofitClientFactory
import com.example.webservice.retrofit.RetrofitWebserviceFactory
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object WebserviceKoinModule {

    private val module = module {

        single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }
        single { RetrofitClientFactory().createOkHttpClient(get()) }
        single { RetrofitWebserviceFactory().createWebservice<MediaWebservice>(get(), BuildConfig.SERVER_URL) }
    }

    fun start() {
        loadKoinModules(module)
    }

}