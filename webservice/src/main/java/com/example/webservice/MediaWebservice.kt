package com.example.webservice

import com.example.webservice.response.FetchMediaDetailResponse
import com.example.webservice.response.FetchMediaUpdatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MediaWebservice {

    @GET("movie/changes")
    fun fetchMediaUpdates(
        @Query("start_date") startDate: String? = null,
        @Query("end_date") endDate: String? = null,
        @Query("page") page: Int = 1,
        @Query("api_key") key: String = BuildConfig.API_KEY
    ): Single<FetchMediaUpdatesResponse>

    @GET("movie/{movie_id}")
    fun fetchMediaDetail(
        @Path("movie_id") id: Long,
        @Query("api_key") key: String = BuildConfig.API_KEY
    ): Single<FetchMediaDetailResponse>

}