package com.example.webservice.response

import com.google.gson.annotations.SerializedName

data class FetchMediaDetailResponse(
    val id: Long,
    val title: String,
    @SerializedName("poster_path")
    val coverImageUrl: String?,
    @SerializedName("adult")
    val isAdult: Boolean
)