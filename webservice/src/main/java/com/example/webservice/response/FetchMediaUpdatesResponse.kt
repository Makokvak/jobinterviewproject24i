package com.example.webservice.response

import com.google.gson.annotations.SerializedName

data class FetchMediaUpdatesResponse(
    val results: List<FetchMediaUpdatesItem>
) {
    data class FetchMediaUpdatesItem(val id: Long, @SerializedName("adult") val isAdult: Boolean)
}